module gitlab.com/diamondburned/6cord

go 1.12

require (
	cloud.google.com/go v0.44.3 // indirect
	github.com/alecthomas/chroma v0.6.3
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/atotto/clipboard v0.1.2
	github.com/coreos/bbolt v1.3.3 // indirect
	github.com/coreos/etcd v3.3.13+incompatible // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f // indirect
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/danieljoos/wincred v1.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/diamondburned/discordgo v1.2.1
	github.com/diamondburned/markdown v0.0.0-20190331092810-a5d3b972382c
	github.com/diamondburned/tcell v1.1.8
	github.com/diamondburned/tview/v2 v2.3.1
	github.com/disintegration/imaging v1.6.0
	github.com/gen2brain/beeep v0.0.0-20190603194150-07ff5e574111
	github.com/go-delve/delve v1.2.0 // indirect
	github.com/go-kit/kit v0.9.0 // indirect
	github.com/go-test/deep v1.0.3
	github.com/gobwas/ws v1.0.2 // indirect
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/golang/groupcache v0.0.0-20190702054246-869f871628b6 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/google/pprof v0.0.0-20190723021845-34ac40c74b70 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/gopherjs/gopherwasm v1.1.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.9.6 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/hashicorp/go-retryablehttp v0.5.4 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jonas747/gojay v0.0.0-20181010205435-9081ac11e06c
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/mattn/go-sixel v0.0.0-20190320171103-a8fac8fa7d81
	github.com/mattn/go-tty v0.0.0-20190424173100-523744f04859
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/peterh/liner v1.1.0 // indirect
	github.com/pkg/profile v1.3.0 // indirect
	github.com/prometheus/client_golang v1.1.0 // indirect
	github.com/prometheus/client_model v0.0.0-20190812154241-14fe0d1b01d4 // indirect
	github.com/rogpeppe/fastuuid v1.2.0 // indirect
	github.com/russross/blackfriday v2.0.0+incompatible // indirect
	github.com/sahilm/fuzzy v0.1.0
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/soniakeys/quant v1.0.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.4.0 // indirect
	github.com/stevenroose/gonfig v0.1.4
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/tadvi/systray v0.0.0-20190226123456-11a2b8fa57af // indirect
	github.com/ugorji/go v1.1.7 // indirect
	github.com/valyala/fasttemplate v1.0.1
	github.com/zalando/go-keyring v0.0.0-20190603084339-02404fc6afd1
	gitlab.com/diamondburned/go-w3m v0.0.0-20190608163716-1b390b8a3d1f
	gitlab.com/diamondburned/ueberzug-go v0.0.0-20190521043425-7c15a5f63b06
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/arch v0.0.0-20190815191158-8a70ba74b3a1 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/mobile v0.0.0-20190814143026-e8b3e6111d02 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/tools v0.0.0-20190816200558-6889da9d5479 // indirect
	google.golang.org/genproto v0.0.0-20190817000702-55e96fffbd48 // indirect
	google.golang.org/grpc v1.23.0 // indirect
	gopkg.in/toast.v1 v1.0.0-20180812000517-0a84660828b2 // indirect
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)
